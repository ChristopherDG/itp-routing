<?php


namespace Gamma\Routing\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\Request\Http;

class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * @var ResponseInterface
     */
    protected $_response;


    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response)
    {
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }

    public function match(RequestInterface $request)
    {
        /** @var Http $request*/
        $identifier = trim($request->getPathInfo(), '/');

        if($identifier === 'routes-testing') {
            return $this->actionFactory->create('Gamma\Routing\Controller\Testing\Index');
        } else {
            return false;
        }
    }
}

