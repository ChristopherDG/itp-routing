<?php


namespace Gamma\Routing\Controller;

use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RequestInterface;


class RouterPages implements RouterInterface
{
    /** @var ActionFactory */
    protected $actionFactory;

    /** @var PageRepositoryInterface */

    protected $pageRepository;

    /** @var SearchCriteriaBuilder  */
    protected $criteriaBuilder;

    public function __construct(
        ActionFactory $actionFactory,
        PageRepositoryInterface $pageRepository,
        SearchCriteriaBuilder $criteriaBuilder
    )
    {
        $this->actionFactory = $actionFactory;
        $this->pageRepository = $pageRepository;
        $this->criteriaBuilder = $criteriaBuilder;
    }

    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        $pages = $this->pageRepository->getList($this->criteriaBuilder->create())->getItems();

        $data = [];


        foreach ($pages as $page) {
            $data[] = $page->getIdentifier();
        }

        if(in_array($identifier, $data)){
            return $this->actionFactory->create('Magento\Cms\Controller\Index\Index');
        }else{
            return null;
        }
    }
}
