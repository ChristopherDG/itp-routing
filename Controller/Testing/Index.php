<?php


namespace Gamma\Routing\Controller\Testing;


use Magento\Framework\App\Action\Action;

class Index extends Action
{
    public function execute()
    {
        die ('<h1>Hello from Routing/Controller/Testing/Index</h1>');
    }
}