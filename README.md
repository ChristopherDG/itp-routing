# Magento 2 ITP module
Custom Routers implementation in magento 2
 
Router:

![RouterResult](https://i.ibb.co/bmWSMZC/Routing-Controller.png)


RouterPages:

![RouterPagesResult](https://i.ibb.co/vqzHnrj/Routing-Pages.png)